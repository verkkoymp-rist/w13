import React from 'react';

function App() {
  const weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

  return (
    <table>
      <thead>
        <tr>
          <th>Weekday</th>
        </tr>
      </thead>
      <tbody>
        {weekdays.map((day) => (
          <tr key={day}>
            <td>{day}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default App;
